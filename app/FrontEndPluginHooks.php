<?php

namespace App;

use Dragon\Hooking\FrontEndPluginHooks as DragonFrontEndPluginHooks;
use Dragon\Session;
use Dragon\Email\Email;

class FrontEndPluginHooks extends DragonFrontEndPluginHooks
{
	protected static $actions = [
		'wp_mail_failed' => [
			[Email::class, 'logFailedMailings'],
		],
		'wp_logout' => [
			[Session::class, 'destroy'],
		],
		'wp_login' => [
			[Session::class, 'destroy'],
		],
		'shutdown' => [
			[FrontEndPluginHooks::class, 'shutdown'],
		],
	];
}
