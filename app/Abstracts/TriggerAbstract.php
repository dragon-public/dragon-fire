<?php

namespace App\Abstracts;

abstract class TriggerAbstract {
	protected static array $triggerManifest = [];
	
	public static function getTriggers() {
		return static::$triggerManifest;
	}
}
