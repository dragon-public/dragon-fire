<?php

namespace App;

use Dragon\Abstracts\AdminMenuAbstract as DragonMenu;

class AdminMenu extends DragonMenu
{
	protected static $rootMenu = [
		'slug'			=> 'dragon-fire-hooks',
		'menu-text'		=> 'Dragon Fire',
		'icon'			=> 'dashicons-welcome-view-site',
		'page-title'	=> 'Hooks',
		'capabilities'	=> 'manage_options',
	];
	
	protected static $submenus = [
		'dragon-fire-hooks' => [
			'slug'			=> 'dragon-fire-hooks',
			'menu-text'		=> 'Hooks',
			'page-title'	=> 'Hooks',
			'capabilities'	=> 'manage_options',
			'callback'		=> 'App\\Pages\\Hooks',
		],
		'dragon-fire-settings' => [
			'slug'			=> 'dragon-fire-settings',
			'menu-text'		=> 'Settings',
			'page-title'	=> 'Settings',
			'capabilities'	=> 'manage_options',
			'callback'		=> 'App\\Pages\\AdminOptions',
		],
		'dragon-fire-log' => [
			'slug'			=> 'dragon-fire-log',
			'menu-text'		=> 'Log',
			'page-title'	=> 'Plugin Log',
			'capabilities'	=> 'manage_options',
			'callback'		=> 'Dragon\\Pages\\AdminLog',
		],
	];
	
	protected static $hiddenMenus = [
		'dragon-fire-hook-details' => [
			'slug'			=> 'dragon-fire-hook-details',
			'menu-text'		=> 'Hook Details',
			'page-title'	=> 'Hook Details',
			'capabilities'	=> 'manage_options',
			'callback'		=> 'App\\Pages\\HookDetails',
		],
	];
}

