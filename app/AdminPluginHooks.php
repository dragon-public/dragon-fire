<?php

namespace App;

use Dragon\Session;
use Dragon\Hooking\AdminPluginHooks as DragonAdminPluginHooks;
use Dragon\Email\Email;

class AdminPluginHooks extends DragonAdminPluginHooks
{
	protected static $actions = [
		'wp_mail_failed' => [
			[Email::class, 'logFailedMailings'],
		],
		'admin_menu' => [
			[AdminMenu::class, 'constructMenus'],
		],
		'wp_logout' => [
			[Session::class, 'destroy'],
		],
		'wp_login' => [
			[Session::class, 'destroy'],
		],
		'shutdown' => [
			[AdminPluginHooks::class, 'shutdown'],
		],
	];
}
