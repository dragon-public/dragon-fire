<?php

namespace App\Pages;

use Dragon\Abstracts\AdminOptionsAbstract;

class AdminOptions extends AdminOptionsAbstract
{
	protected array $sections = [
		'Plugin Settings' => [
			'dragon_app_remove_tables_on_deactivation'	=> [
				'type'		=> 'select',
				'required'	=> true,
				'label'		=> 'Remove all plugin tables on deactivation?',
				'options'	=> [
					'yes'	=> 'Yes',
					'no'	=> 'No (Recommended)',
				],
			],
		],
	];
}
