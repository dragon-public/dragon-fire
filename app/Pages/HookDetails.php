<?php

namespace App\Pages;

use Dragon\Abstracts\DetailsAbstract;
use App\Models\Hook;
use App\Mapper;

class HookDetails extends DetailsAbstract
{
	protected string $listPageSlug = "dragon-fire-hooks";
	protected string $modelName = Hook::class;
	
	protected array $dbColumnToField = [
		'active'	=> [
			'type'		=> 'checkbox',
			'label'		=> 'Active?',
			'attributes'	=> [
				'value'		=> 1,
				'checked'	=> 'checked',
			],
			'description' => 'Should this trigger/action combo be enabled on your site?',
		],
		'trigger'	=> [
			'type'		=> 'select',
			'required'	=> true,
			'label'		=> 'Trigger',
			'options'	=> [],
			'description' => 'What should cause something to occur?',
		],
		'action'	=> [
			'type'		=> 'select',
			'required'	=> true,
			'label'		=> 'Action',
			'options'	=> [],
			'description' => 'What should happen?',
		],
	];
	
	protected function setDbColumnToField() {
		$this->setActiveStatus();
		$this->setTriggers();
	}
	
	private function setActiveStatus() {
		$model = $this->maybeGetModel();
		if (!empty($model)) {
			$this->dbColumnToField['active']['attributes']['value'] = (int)$model->active;
			if (!$model->active) {
				unset($this->dbColumnToField['active']['attributes']['value']['checked']);
			}
		}
	}
	
	private function setTriggers() {
		$usedTriggers = Hook::all()->pluck('trigger')->toArray();
		$availableTriggers = Mapper::listTriggers();
		foreach ($availableTriggers as $name => $data) {
			if (in_array($name, $usedTriggers)) {
				continue;
			}
			$triggers[$name] = '[' . explode('|', $name)[0] . '] ' . $data['description'];
		}
		
		if (empty($triggers)) {
			$this->dbColumnToField['trigger']['options'] = [
				'' => '[No triggers available]',
			];
		} else {
			natsort($triggers);
			$this->dbColumnToField['trigger']['options'] = $triggers;
		}
	}
}
