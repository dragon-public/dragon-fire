<?php

namespace App\Pages;

use Dragon\Abstracts\TableAbstract;
use App\Models\Hook;

class Hooks extends TableAbstract
{
	protected $createPageSlug = "dragon-fire-hook-details";
	protected $updatePageSlug = "dragon-fire-hook-details";
	protected $currentPageSlug = "dragon-fire-hooks";
	
	protected $pageText = "Create, view, update, and delete hooks";
	
	protected $canCreate = true;
	protected $canUpdate = true;
	protected $canDelete = true;
	
	protected $headers = [
		'Date Time Added',
		'Trigger',
		'Action',
		'Active?',
	];
	
	protected $rows = [];
	
	protected function getModelToDelete($id)
	{
		return Hook::find($id);
	}
	
	protected function getModelQuery()
	{
		return Hook::orderBy('trigger', 'ASC')->orderBy('action', 'ASC');
	}
	
	protected function addRow($model)
	{
		$this->rows[] = [
			[
				'data'	=> $model->created_at,
			],
			[
				'data'	=> $model->trigger,
			],
			[
				'data'	=> $model->action,
			],
			[
				'data'	=> $model->active ? ' Active' : 'Inactive',
			],
		];
	}
}
