<?php

namespace App;

class Mapper {
	public static function listTriggers() {
		$triggers = [];
		static::iterateDirectory(__DIR__ . '/Triggers', function ($triggerClass) use(&$triggers) {
			$currentTriggers = call_user_func(['\\App\Triggers\\' . $triggerClass, 'getTriggers']);
			$triggers = array_merge($triggers, $currentTriggers);
		});
		
		return $triggers;
	}
	
	public static function listActions() {
		/**
		 * Given that some trigger data might get used twice in an action, and
		 * not all items may be available on the trigger, or required for an
		 * action, there's just too many problems with restricting the number
		 * of actions shown for a given trigger. So, just list them all.
		 * 
		 * If the user can't fill all of the fields, then they can't add the
		 * action.
		 */
	}
	
	private static function iterateDirectory(string $dir, Callable $callback)
	{
		$dir = dir($dir);
		while (false !== ($entry = $dir->read())) {
			
			if (in_array($entry, ['.', '..']) || strpos($entry, '.php') === false) {
				continue;
			}
			
			$className = str_replace('.php', '', $entry);
			$callback($className);
			
		}
		
		$dir->close();
	}
}
