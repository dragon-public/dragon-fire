<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hook extends Model
{
	protected $casts = [
		'active' => 'boolean',
	];
	
	protected $table = 'dragon_fire_hooks';
}
