<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
	public $timestamps = false;
	
	protected $primaryKey = 'ID';
	
	protected $casts = [
		'user_registered' => 'datetime',
	];
	
	public function meta()
	{
		return $this->hasMany(UserMeta::class, 'user_id');
	}
}
