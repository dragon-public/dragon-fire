<?php

namespace App\Triggers;

use App\Abstracts\TriggerAbstract;

class WordPressTriggers extends TriggerAbstract {
	protected static array $triggerManifest = [
		'WordPress|OnSignup' => [
			'description' => 'Do something when a user signs up on your site.',
			'values' => [
				'username' => [
					'description'	=> 'Username',
					'type'			=> 'string',
				],
				'password' => [
					'description'	=> 'Password',
					'type'			=> 'string',
				],
			],
		],
	];
	
	public static function prepareOnSignup() {
		add_action('user_register', [static::class, 'handleOnSignup']);
	}
	
	public static function handleOnSignup(int $userId) {
		/**
		 * Don't return anything, but instead run the action attached to it in the DB.
		 */
		return [
			'username' => 'Test',
			'password' => 'Blah',
		];
	}
}
