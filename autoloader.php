<?php

require_once(__DIR__ . '/vendor/autoload.php');

spl_autoload_register(function ($className)
{
	$ns = 'App\\';
	if (strpos($className, $ns) !== false) {

		$noNamespace = str_replace($ns, '', $className);
		$normalizedClassName = str_replace('\\', '/', $noNamespace);
		$fullPathFile = __DIR__ . '/app/' . $normalizedClassName . '.php';

		if (($realPath = realpath($fullPathFile)) !== false) {
			require_once($realPath);
		}

	}
});
