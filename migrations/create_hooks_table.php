<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Capsule\Manager as Capsule;
use Dragon\Abstracts\MigrationAbstract;

class CreateHooksTable extends MigrationAbstract
{
	public $tableName = 'dragon_fire_hooks';
	
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Capsule::schema()->create($this->tableName, function (Blueprint $table) {
    		
    		$table->bigIncrements('id');
    		$table->boolean('active')->default(true);
    		$table->string('trigger')->unique();
    		$table->string('action');
    		$table->json('meta')->nullable();
    		
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Capsule::schema()->drop($this->tableName);
    }
}
