<?php

namespace Tests;

use Dragon\DB;

class TestCase extends \WP_UnitTestCase {
	public function setUp() {
		$db = DB::make();
		$db->migrate();
	}
	
	public function tearDown() {
		$db = DB::make();
		$db->rollback();
	}
}
