<?php

use Dragon\Config;
use App\AdminPluginHooks;
use App\FrontEndPluginHooks;
use Dragon\Ignite;

/**
 * @package Dragon Fire
 * 
 * Plugin Name: Dragon Fire
 * Description: Automate various tasks in your system when an event occurs.
 * Version: 1.0.0
 * Author: Red Scale Corporation
 * Text Domain: dragon-fire
 * License: MIT
 **/

require_once('autoloader.php');

Config::$namespace = 'dragon-fire';
Config::$pluginName = 'dragon-fire';
Config::$pluginBaseUrl = plugin_dir_url(__FILE__);
Config::$pluginLoaderFile = __FILE__;
Config::$pluginDir = __DIR__;
Ignite::fire();

if (is_admin()) {
	
	register_activation_hook(Config::$pluginLoaderFile, array(AdminPluginHooks::class, 'onActivation'));
	register_deactivation_hook(Config::$pluginLoaderFile, array(AdminPluginHooks::class, 'onDeactivation'));
	
	add_action('init', array(AdminPluginHooks::class, 'init'));
	
} else {
	add_action('init', array(FrontEndPluginHooks::class, 'init'));
}

if (!function_exists('do_output_buffer')) {
	
	function do_output_buffer() {
		ob_start();
	}
	
	add_action('init', 'do_output_buffer');
	
}
